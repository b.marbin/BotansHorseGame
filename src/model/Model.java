package model;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class Model extends Component{

    // FINALS
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    // Eigenschaften


    private String gewinnerPferd;
    private String name;
    private List<Horse> horses = new LinkedList<>();
    private Player player;
    boolean gewinner = false;
    private boolean gameOver = false;
    private boolean start = false;


    // Konstruktoren
    public Model() {
        this.horses.add(new Horse(40, 75, "SCHIMMEL ist "));
        this.horses.add(new Horse(40, 175, "FOREST ist "));
        this.horses.add(new Horse(40, 275, "BUTTERCUP ist "));
        //   this.horses.add(new Horse(40, 375, "CHOPPEK"));
        this.player = new Player(40, 375);

    }


    // Methoden

    public void update (long elapsedTime) {
        for (Horse horse : horses){
            horse.update((int) elapsedTime);
        }
    }

    public void collisionDetection (){
        Rectangle spielFeldRand = new Rectangle(800, 0, 10, 600 );
        for (Horse horse : this.horses) {
            Rectangle pferdeKoerper = new Rectangle(horse.getX(), horse.getY(), 20, 40);
            Rectangle spieler = new Rectangle(player.getX(), player.getY(), player.getW(), player.getH());
            if (spielFeldRand.intersects(pferdeKoerper)){
                this.gewinnerPferd = horse.getName();
                gewinner=true;
                this.gameOver = true;
            }
            else if (spielFeldRand.intersects(spieler)) {
                gewinner = true;
                this.gewinnerPferd = player.getName();
             this.gameOver = true;

        }
            }

        }
    // System.exit(10);

    // Setter + Getter


    public String getGewinnerPferd() {
        return gewinnerPferd;
    }

    public boolean isGewinner() {
        return gewinner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Horse> getHorses() {
        return horses;
    }

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean start) {
        this.start = start;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public Player getPlayer() {
        return player;
    }
}
