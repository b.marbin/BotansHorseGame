package model;

import java.util.Random;

public class Horse {

    // Eigenschaften
    private Random r = new Random();
    private String name;
    private int x;
    private int y;
    private int h;
    private int w;
    private double speedX;

    private int bla;
    // Konstruktoren
    public Horse(int x, int y, String name) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.h = 60;
        this.w = 100;
        this.speedX = Math.random() * 0.3;
        if (speedX < 0.1) {
            speedX += 0.1;
        }
    }

    // Methoden
    public void update(int elapsedTime) {
        this.x = (int) Math.round(this.x + elapsedTime * speedX);
    }

    // Setter + Getter




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public double getSpeedX() {
        return speedX;
    }
}
