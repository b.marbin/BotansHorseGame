package model;

public class Player {


    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;
    private double speedX;
    private String name = "Du bist ";

    // Konstruktoren
    public Player(int x, int y) {

        this.name = getName();

        this.x = x;
        this.y = y;
        this.h = 20;
        this.w = 40;
        this.speedX = Math.random() * 0.5;
        if (speedX < 0.1) {
            speedX += 0.1;
        }
    }


    // Methoden
    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }


    // Getter + Setter


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public String getName() {
        return name;
    }
}
