import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Horse;
import model.Model;

public class Graphics extends VBox {

    // Eigenschaften
    private InputHandler inputHandler = null;
    private Model model;
    private GraphicsContext gc;
    private ComboBox<String> horsePicker = new ComboBox<>(FXCollections.observableArrayList(
            "WINNIPEG", "SATOMI", "BUTTERCUP", "CHOPPEK"

    ));




    // Konstruktoren
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    // Methoden
    public void draw() {

        // Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);



        //Draw Background
        gc.setFill(Color.LIGHTGREEN);
        gc.fillRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.setFill(Color.SANDYBROWN);
        gc.fillRect(0, 80, Model.WIDTH, 50);
        gc.setFill(Color.SANDYBROWN);
        gc.fillRect(0, 180, Model.WIDTH, 50);
        gc.setFill(Color.SANDYBROWN);
        gc.fillRect(0, 280, Model.WIDTH, 50);
        gc.setFill(Color.SANDYBROWN);
        gc.fillRect(0, 350, Model.WIDTH, 50);


        // Draw Horses
        Image pferd = new Image("Bild/horse.png");

        for (Horse horse : this.model.getHorses()) {
            gc.drawImage(pferd, horse.getX(), horse.getY(),  horse.getW() ,horse.getH());

        }


        // Draw Player

        gc.setFill(Color.YELLOW);
        gc.fillRect(
                model.getPlayer().getX() - model.getPlayer().getW() / 2,
                model.getPlayer().getY() - model.getPlayer().getH() / 2,
                model.getPlayer().getW(),
                model.getPlayer().getH()
        );
    }

    public void drawStartOfGame() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.setFill(Color.GREEN);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(2);
        Font theFont = Font.font("Times New Roman", FontWeight.BOLD, 48);
        gc.setFont(theFont);
        gc.fillText("Drücke Enter", 200, 300);

    }




    public void drawEndOfGame() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.setFill(Color.GREEN);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(2);
        Font theFont = Font.font("Times New Roman", FontWeight.BOLD, 48);
        gc.setFont(theFont);
        gc.fillText(model.getGewinnerPferd() + "der Gewinner!", 100, 300);



    }

}
